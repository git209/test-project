FROM python:3.10-slim-buster

RUN mkdir -p /src
COPY src/ /src/

ENV PYTHONDONTWRITEBYTECODE=1
ENV FLASK_DEBUG=1
ENV PYTHONUNBUFFERED=1

WORKDIR /src

RUN pip install -r requirements.txt

ENTRYPOINT [ "flask", "run", "--host=0.0.0.0"]